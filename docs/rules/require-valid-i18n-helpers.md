# @gitlab/require-valid-i18n-helpers

This rule enforces valid usages of the translation helpers (`__`, `s__` and `n__`). It requires the
following:

- All helpers should be called with string-literal arguments to ensure the strings are properly
  collected by the `gettext:regenerate` script.
- `s__` requires a translation namespace to be provided.

## Rule Details

### Examples of **incorrect** code for this rule

```js
// Invalid argument types
const label = __(LABEL);
const description = __(getDescription());

// Missing translation namespace
const namespacedTranslation = s__('NamespacedTranslation');
```

### Examples of **correct** code for this rule

```js
// Valid argument type
const label = __('Some label');

// Valid translation namespace
const namespacedTranslation = s__('Namespace|Translation');
// or
const otherNamespacedTranslation = s__('Namespace', 'Other translation');
```

## Options

Nothing

## Related rules

- TBA
