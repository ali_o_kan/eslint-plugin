## Installation

You'll first need to install [ESLint](http://eslint.org):

```bash
yarn add --dev eslint
```

Next, install `@gitlab/eslint-plugin`:

```bash
yarn add --dev @gitlab/eslint-plugin
```

**Note:** If you installed ESLint globally (using the `-g` flag) then you must also install `@gitlab/eslint-plugin` globally.

## Usage (configs)

### `plugin:@gitlab/default`

Add `plugin:@gitlab/default` to the extends section of your `.eslintrc` configuration file.
This config is for our common javascript and Vue rules.

```yaml
extends:
  - 'plugin:@gitlab/default'
```

### `plugin:@gitlab/i18n`

Add `plugin:@gitlab/i18n` to the extends section of your `.eslintrc` configuration file.
This config is for i18n linting on the GitLab project.

```yaml
extends:
  - 'plugin:@gitlab/i18n'
```

### `plugin:@gitlab/jest`

Add `plugin:@gitlab/jest` to the extends section of your `.eslintrc` configuration file.
This config is for Jest rules.

```yaml
extends:
  - 'plugin:@gitlab/jest'
```

## Usage (rules)

Add `@gitlab` to the plugins section of your `.eslintrc` configuration file:

```yaml
plugins:
  - @gitlab
```

Then configure the rules you want to use under the rules section, for example:

YAML:

```yaml
rules:
  '@gitlab/vue-require-i18n-attribute-strings': error
```

## VueJS (.vue files)

`@gitlab/eslint-plugin` can also detect strings requiring externalization for code within the `<script></script>` tags of a **.vue** file.
