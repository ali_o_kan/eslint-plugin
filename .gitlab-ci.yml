include:
  - project: 'gitlab-org/frontend/frontend-build-images'
    file: '/semantic-release/.gitlab-ci-template.rules.yml'
  - project: 'gitlab-org/frontend/untamper-my-lockfile'
    file: 'templates/merge_request_pipelines.yml'

variables:
  REGISTRY_HOST: 'registry.gitlab.com'
  REGISTRY_GROUP: 'gitlab-org'
  GRAPHQL_SCHEMA_APOLLO_FILE: 'gitlab_schema_apollo.graphql'

stages:
  - test
  - deploy
  - integration

workflow:
  rules:
    - if: $CI_MERGE_REQUEST_IID
    - if: $CI_COMMIT_TAG
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

default:
  tags:
    - gitlab-org
  image: node:16
  cache:
    paths:
      - node_modules/

test:
  stage: test
  script:
    - yarn install --frozen-lockfile --check-files
    - yarn test

.integration-test:
  needs: ['test']
  stage: integration
  before_script:
    - ./scripts/integration_test/bootstrap.sh
  allow_failure: true

generate-apollo-graphql-schema:
  stage: integration
  needs: ['test']
  image:
    name: ${REGISTRY_HOST}/${REGISTRY_GROUP}/gitlab-build-images:apollo
    entrypoint: ['']
  script:
    - apollo client:download-schema --endpoint=https://gitlab.com/api/graphql ${GRAPHQL_SCHEMA_APOLLO_FILE}
  artifacts:
    name: graphql-schema-apollo
    paths:
      - '${GRAPHQL_SCHEMA_APOLLO_FILE}'

test-on-gitlab:
  extends: .integration-test
  needs: ['generate-apollo-graphql-schema']
  variables:
    GRAPHQL_SCHEMA_APOLLO_DIR: 'tmp/tests/graphql'
    TARGET_REPO: https://gitlab.com/gitlab-org/gitlab.git
  script:
    - mkdir -p "src/${GRAPHQL_SCHEMA_APOLLO_DIR}"
    - mv "${GRAPHQL_SCHEMA_APOLLO_FILE}" "src/${GRAPHQL_SCHEMA_APOLLO_DIR}/${GRAPHQL_SCHEMA_APOLLO_FILE}"
    - cd src
    - yarn lint:eslint:all --parser-options="schema:${GRAPHQL_SCHEMA_APOLLO_DIR}/${GRAPHQL_SCHEMA_APOLLO_FILE}"

test-on-gitlab-ui:
  extends: .integration-test
  variables:
    TARGET_REPO: https://gitlab.com/gitlab-org/gitlab-ui.git
    # Do not install cypress or puppeteer
    CYPRESS_INSTALL_BINARY: 0
    PUPPETEER_SKIP_DOWNLOAD: 'true'
  script:
    - cd src
    - yarn run prebuild
    - yarn eslint

semantic-release:
  extends: .semantic-release

semantic-release-dry-run:
  extends: .semantic-release
  needs: []
  script: semantic-release --branches $CI_MERGE_REQUEST_SOURCE_BRANCH_NAME --dry-run --no-ci
  rules:
    - if: $CI_MERGE_REQUEST_IID && $CI_MERGE_REQUEST_PROJECT_ID == $CI_MERGE_REQUEST_SOURCE_PROJECT_ID
