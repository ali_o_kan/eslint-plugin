// ------------------------------------------------------------------------------
// Requirements
// ------------------------------------------------------------------------------

const rule = require('../../../lib/rules/require-valid-i18n-helpers');
const RuleTester = require('eslint').RuleTester;
const {
  STRING_LITERALS_ERRORS,
  STRING_LITERALS_VALID_CASES,
  STRING_LITERALS_INVALID_CASES,
  NAMESPACED_TRANSLATION_VALID_CASES,
  NAMESPACED_TRANSLATION_INVALID_CASES,
  NAMESPACED_TRANSLATION_ERRORS,
} = require('./require-valid-i18n.constants');

// ------------------------------------------------------------------------------
// Tests
// ------------------------------------------------------------------------------

const ruleTester = new RuleTester({
  parser: require.resolve('vue-eslint-parser'),
  parserOptions: { ecmaVersion: 2015 },
});

ruleTester.run('require-valid-i18n-helpers', rule, {
  valid: [...STRING_LITERALS_VALID_CASES, ...NAMESPACED_TRANSLATION_VALID_CASES],

  invalid: [
    ...STRING_LITERALS_INVALID_CASES.map((code) => ({
      code,
      errors: STRING_LITERALS_ERRORS,
    })),
    ...NAMESPACED_TRANSLATION_INVALID_CASES.map((code) => ({
      code,
      errors: NAMESPACED_TRANSLATION_ERRORS,
    })),
  ],
});
