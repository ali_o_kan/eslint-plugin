const {
  getFunctionName,
  isString,
  isTemplateLiteral,
  getTemplateLiteralString,
} = require('../utils/rule-utils');

const GETTEXT = '__';
const P_GETTEXT = 's__';
const N_GETTEXT = 'n__';

const ERROR_MESSAGE_STRING_LITERAL = 'Translation helpers must be called with a string literal.';
const ERROR_MESSAGE_MISSING_NAMESPACE = 's__ requires a translation namespace to be provided.';

const mustBeCalledWithStringLiteralArguments = (context, node) => {
  const report = (node) => {
    context.report({
      node,
      message: ERROR_MESSAGE_STRING_LITERAL,
    });
  };

  if (!node.arguments.length) {
    report(node);
  }
  const [arg1, arg2] = node.arguments;
  if ([arg1, arg2].some((arg) => arg && !isString(arg))) {
    report(node);
  }
};

const mustHaveNamespace = (context, node) => {
  if (node.arguments.length === 2) {
    return;
  }

  const [arg] = node.arguments;
  const value = isTemplateLiteral(arg) ? getTemplateLiteralString(arg).trim() : arg.value;
  if (!/^\w.+\|/.test(value)) {
    context.report({
      node,
      message: ERROR_MESSAGE_MISSING_NAMESPACE,
    });
  }
};

const VALIDATORS = {
  [GETTEXT]: [mustBeCalledWithStringLiteralArguments],
  [P_GETTEXT]: [mustBeCalledWithStringLiteralArguments, mustHaveNamespace],
  [N_GETTEXT]: [mustBeCalledWithStringLiteralArguments],
};

const validateI18nHelperCallFactory = (context) => (node) => {
  const functionName = getFunctionName(node);
  if (!Object.keys(VALIDATORS).includes(functionName)) {
    return;
  }

  VALIDATORS[functionName].forEach((validator) => {
    validator(context, node);
  });
};

module.exports = {
  validateI18nHelperCallFactory,
  ERROR_MESSAGE_STRING_LITERAL,
  ERROR_MESSAGE_MISSING_NAMESPACE,
};
